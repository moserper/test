import React, { Component, Fragment } from 'react';
import List from './components/List';
import MainList from './components/MainList';

function App() {
  return <div>
    <MainList />
  </div>
}

export default App;
