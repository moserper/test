import React from 'react'
import { Row, Col, Button } from 'react-bootstrap';
import List from './List';
import './AddList.css';
import { TodolistServices } from '../services/TodolistServices';

const styles = {
  row: {
    paddingRight: 0,
    paddingLeft: 0,
    marginTop: "1px",
  },
  col: {
    paddingLeft: 0,
  },

};

class AddList extends React.Component {

  constructor(props) {
    super(props);
    this.handleFilterTextNameChange = this.handleFilterTextNameChange.bind(this);
    this.handleFilterTextNumberChange = this.handleFilterTextNumberChange.bind(this);
    this.handleOnClickButton = this.handleOnClickButton.bind(this);
    this.handleRemove = this.handleRemove.bind(this);
    this.handleCompleted = this.handleCompleted.bind(this);
    this.handleOnSave = this.handleOnSave.bind(this);
  }

  handleFilterTextNumberChange(e) {
    this.props.onFilterTextNumber(e.target.value);
  }

  handleFilterTextNameChange(e) {
    this.props.onFilterTextName(e.target.value);
  }

  handleAddArrayList(newList) {
    this.props.onAddArrayList(newList);
  }

  handleCompleted(id, key, completed) {
    TodolistServices.completed(id, completed).then((result) => {
      console.log(result)
      this.props.list[key].list_completed = completed;
      this.handleAddArrayList(this.props.list);
    }).catch((err) => {
      window.alert(err);
      console.log(err)
    });
  }

  handleOnSave(id, name, key) {
    TodolistServices.edit(id, name).then((result) => {
      console.log(result)
      this.props.list[key].list_name = name;
      this.handleAddArrayList(this.props.list);
    }).catch((err) => {
      window.alert(err);
      console.log(err)
    });
  }

  handleRemove(number, id) {
    // remove in database
    TodolistServices.remove(id).then(result => {
      if (result.message == "remove todolist success") {
        let index = this.serachDuplicateNumber(number, this.props.list)
        this.props.list.splice(index, 1);
        this.handleAddArrayList(this.props.list);
      } else {
        window.alert('server error');
      }
    }).catch((err) => {
      window.alert(err);
    })

  }

  setName(textName) {
    this.props.onFilterTextName(textName);
  }

  setNumber(textNumber) {
    this.props.onFilterTextNumber(textNumber);
  }

  to(promise) {
    return promise.then(data => {
      console.log(data);
      return [null, data];
    }).catch(err => [err]);
  }

  handleValildation() {
    let formIsVaild = true;
    let error = {};
    if (!this.props.textName) {
      formIsVaild = false;
      error.name = "Name Cannot be empty";
    }

    if (!this.props.textNumber) {
      formIsVaild = false;
      error.number = "Number Cannot be empty";
    } else if (!this.props.textNumber != "undefined") {
      if (!this.props.textNumber.toString().match(/[0-9]+/)) {
        formIsVaild = false;
        error.number = "Only Number";
      }
    }
    error.formIsValid = formIsVaild;
    return error;
  }

  serachDuplicateNumber(number, lists) {
    let l = 0;
    let r = lists.length - 1;
    while (l <= r) {
      let m = Math.floor((l + r) / 2);
      if (lists[m].list_number < number) {
        l = m + 1;
      } else if (lists[m].list_number > number) {
        r = m - 1;
      } else {
        return m;
      }
    }
    return -1;
  }

  serachAddList(number, lists) {
    let l = 0;
    let r = lists.length - 1;
    while (l <= r) {
      let m = Math.floor((l + r) / 2);
      if (lists[m].list_number < number) {
        l = m + 1;
      } else if (lists[m].list_number > number) {
        r = m - 1;
      } else {
        return m;
      }
    }
    return l;
  }

  handleOnClickButton() {
    let error = this.handleValildation();
    let alert = '';
    if (!error.formIsValid) {
      for (let err in error) {
        if (typeof error[err] != "boolean")
          alert += error[err] + ' and ';
      }
      window.alert(alert);
      this.setName('');
      this.setNumber('')
    } else {
      if (this.serachDuplicateNumber(this.props.textNumber, this.props.list) == -1) {
        let error, result;
        (async () => {
          [error, result] = await this.to(TodolistServices.create(parseInt(this.props.textNumber), this.props.textName));
          // console.log("error" + error);
          // console.log(result);
          if (error) {
            window.alert(error);
            this.setNumber('');
            return;
          }
          console.log(result.data.id)
          let todolist_id = result.data.id;
          let index = this.serachAddList(this.props.textNumber, this.props.list);
          this.props.list.splice(index, 0, { id: todolist_id, list_number: parseInt(this.props.textNumber), list_name: this.props.textName, list_completed: false })
          this.handleAddArrayList(this.props.list);
        })();
      } else {
        window.alert("number is duplicate");
        this.setNumber('')
      }
    }
  }

  render() {
    const arrayList = [];
    this.props.list.forEach((todolist, index) => {
      arrayList.push(
        <List
          key={index}
          id={todolist.id}
          list_name={todolist.list_name}
          list_number={todolist.list_number}
          list_completed={todolist.list_completed}
          onRemove={this.handleRemove}
          onSave={this.handleOnSave}
          onComplete={this.handleCompleted}
        />
      )
    })

    return <div>
      <div className="addList">
        <Row>
          <Col style={styles.col}>
            <input type="text" size="8" value={this.props.textNumber} onChange={this.handleFilterTextNumberChange} placeholder="Add number" />
          </Col>
          <Col style={styles.col} xs={8}>
            <input type="text" size="55" value={this.props.textName} onChange={this.handleFilterTextNameChange} placeholder="Add list" />
          </Col>
          <Col style={{ paddingLeft: "22px" }}>
            <Button onClick={this.handleOnClickButton} size="lg" variant="light" block>
              <strong>Add</strong>
            </Button>
          </Col>
        </Row>
      </div>
      <div>
        {arrayList}
      </div>
    </div>


  }
}

export default AddList;