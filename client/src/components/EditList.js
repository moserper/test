import React from 'react'
import { Row, Col, Button } from 'react-bootstrap';
import { Icon } from 'semantic-ui-react'
import './AddList.css';

const styles = {
  row: {
    paddingRight: "0px",
    paddingLeft: "0px",
    marginTop: "1px"
  },
  col: {
    paddingLeft: "0px",
    paddingRight: "0px",
  },
  border: {
    border: "none"
  },
  middle: {
    marginLeft: "20px"
  }
};


class EditList extends React.Component {
  constructor(props) {
    super(props);
    this.handleCancelEdit = this.handleCancelEdit.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.handleOnChangeName = this.handleOnChangeName.bind(this);
  }
  handleCancelEdit() {
    this.props.onCancelEdit();
  }

  handleSave() {
    this.props.onSave();
  }

  handleOnChangeName(e) {
    this.props.onChangeName(e.target.value);
  }

  render() {
    return <div className="addList">
      <Row style={{ marginTop: "20px" }}>
        <Col style={styles.col}>
          <input type="text" size="8" disabled={false} defaultValue={this.props.list_number} style={styles.border} />
        </Col>
        <Col style={{ marginLeft: "2px" }} xs={8}>
          <input type="text" size="55" defaultValue={this.props.list_name} onChange={this.handleOnChangeName} />
        </Col>
        <Col style={styles.middle}>
          <Row style={styles.row}>
            <Col style={styles.col}>
              <Button size="lg" variant="light" onClick={this.handleSave} ><strong>Save</strong></Button>
            </Col>
            <Col style={styles.col}>
              <Button icon="true" onClick={this.handleCancelEdit}>
                <Icon name="delete"></Icon>
              </Button>
            </Col>
          </Row>
        </Col>
      </Row>
    </div >
  }
}

export default EditList;

