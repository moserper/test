import React from 'react';
import ShowList from './ShowList';
import EditList from './EditList';
class List extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isEdit: false,
      newListName: '',
    }
    this.handleRemove = this.handleRemove.bind(this);
    this.handleComplete = this.handleComplete.bind(this);
    this.handleIsEdit = this.handleIsEdit.bind(this);
    this.handleOnSave = this.handleOnSave.bind(this);
    this.handleOnChangeName = this.handleOnChangeName.bind(this);
  }

  handleRemove() {
    this.props.onRemove(this.props.list_number, this.props.id);
  }

  handleComplete(completed) {
    this.props.onComplete(this.props.id, this._reactInternalFiber.key, completed);
  }

  handleIsEdit() {
    this.setState(state => { return { isEdit: !state.isEdit } })
  }

  handleOnChangeName(name) {
    this.setState({ newListName: name })
  }

  handleOnSave() {
    let newListName = this.state.newListName ? this.state.newListName : this.props.list_name;
    this.props.onSave(this.props.id, newListName, this._reactInternalFiber.key)
    this.setState(state => {
      return { isEdit: !state.isEdit, newListName: '' }
    })
  }

  render() {
    return <div>
      {
        this.state.isEdit
          ? <EditList
            id={this.props.id}
            list_name={this.props.list_name}
            list_number={this.props.list_number}
            list_completed={this.props.list_completed}
            onSave={this.handleOnSave}
            onCancelEdit={this.handleIsEdit}
            onChangeName={this.handleOnChangeName}
          />
          : <ShowList
            id={this.props.id}
            list_name={this.props.list_name}
            list_number={this.props.list_number}
            list_completed={this.props.list_completed}
            onRemove={this.handleRemove}
            onComplete={this.handleComplete}
            onEdit={this.handleIsEdit}
          />
      }
    </div>
  }
}


export default List;
