import React from 'react';
import AddList from './AddList';
import { TodolistServices } from '../services/TodolistServices';
import './MainList.css';

class MainList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      number: '',
      name: '',
    }
  }

  handleAddArrayList = (lists) => {
    this.setState({ list: lists, number: '', name: '' });
  }

  handleFilterTextNumberChange = (filterText) => {
    this.setState({ number: filterText })
  }

  handleFilterTextNameChange = (filterText) => {
    console.log(filterText);
    this.setState({ name: filterText })
  }


  componentDidMount() {
    TodolistServices.getAll().then((result) => {
      let datas = result.data;
      if (datas) {
        this.setState({ list: datas });
      }
    }).catch((err) => {
      console.log(err);
    });
  }

  render() {
    return <div className="main">
      <div className="list">
        <h2><strong>To-Do List</strong></h2>
        <AddList
          list={this.state.list ? this.state.list : []}
          textNumber={this.state.number}
          textName={this.state.name}
          onAddArrayList={this.handleAddArrayList}
          onFilterTextNumber={this.handleFilterTextNumberChange}
          onFilterTextName={this.handleFilterTextNameChange}
        />
      </div>
    </div>
  }
}

export default MainList;