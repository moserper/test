import React from 'react'
import '../App.css';
import { Checkbox, Icon, Button } from 'semantic-ui-react'

class ShowList extends React.Component {
  constructor(props) {
    super(props);
    this.handleRemove = this.handleRemove.bind(this);
    this.handleComplete = this.handleComplete.bind(this);
    this.handleIsEdit = this.handleIsEdit.bind(this);
  }

  handleRemove() {
    this.props.onRemove();
  }

  handleComplete() {
    this.props.onComplete(!this.props.list_completed);
  }

  handleIsEdit() {
    this.props.onEdit();
  }

  render() {
    return <div className="List">
      <Checkbox type="checkbox" onChange={this.handleComplete} className="custom-input" defaultChecked={this.props.list_completed} />
      <p>{this.props.list_number}</p>
      <p>{this.props.list_name}</p>
      <div className="List-left">
        <Button icon onClick={this.handleIsEdit}>
          <Icon size="large" name="edit outline"></Icon>
        </Button>

        <Button icon onClick={this.handleRemove} >
          <Icon size="large" name="delete" ></Icon>
        </Button>
      </div>
    </div>
  }
}

export default ShowList;
