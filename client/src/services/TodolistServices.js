import { ResponsiveEmbed } from "react-bootstrap";

function create(number, name) {
  let post_data = JSON.stringify({ number, name });
  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      'Content-Length': Buffer.byteLength(post_data),
      'Accept': 'application/json',
    },
    body: post_data
  };
  return fetch('/api/list/create', requestOptions).then(handleResponse)
}

function edit(id, name) {
  const requestOptions = {
    method: 'PATCH',
  }
  return fetch(`/api/list/edit/${id}/${name}`, requestOptions).then(handleResponse)
}

function remove(id) {
  const requestOptions = {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      'Accept': 'application/json',
    },
  }
  return fetch(`/api/list/remove/${id}`, requestOptions).then(handleResponse)
}

function completed(id, completed) {
  const requestOptions = {
    method: 'PATCH',
  }
  return fetch(`/api/list/updateComplete/${id}/${completed}`, requestOptions).then(handleResponse)
}

function getAll() {
  const requestOptions = {
    method: 'GET',
  }
  return fetch('/api/list/getToDoList', requestOptions).then(handleResponse)
}

function handleResponse(response) {
  return response.text().then((result) => {
    const data = result && JSON.parse(result);
    console.log(data);
    if (!response.ok) {
      const error = (data && data.error) || ResponsiveEmbed.statusText;
      return Promise.reject(error);
    }
    return data;
  })
}

export const TodolistServices = {
  create,
  edit,
  remove,
  completed,
  getAll
}