const Pool = require("pg").Pool;
require("dotenv").config();

const connectionString = {
  user: process.env.DB_USER,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD, 
  port: process.env.DB_PORT,
  host: process.env.DB_HOST,
  connectionString: function () {
    return `postgresql://${this.user}:${this.password}@${this.host}:${this.port}/${this.database}`;
  },
};

const pool = new Pool({
  connectionString: connectionString.connectionString(),
});

module.exports = { pool };
