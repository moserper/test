PGDMP     8                	    x            Todolist_Database    12.4 (Debian 12.4-1.pgdg100+1)    12.4     a           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            b           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            c           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            d           1262    16386    Todolist_Database    DATABASE     �   CREATE DATABASE "Todolist_Database" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8';
 #   DROP DATABASE "Todolist_Database";
                admin    false            �            1259    16389    todolist    TABLE     �   CREATE TABLE public.todolist (
    id integer NOT NULL,
    list_number integer NOT NULL,
    list_name character varying NOT NULL,
    list_completed boolean NOT NULL
);
    DROP TABLE public.todolist;
       public         heap    admin    false            �            1259    16397    todolist_id_seq    SEQUENCE     �   CREATE SEQUENCE public.todolist_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.todolist_id_seq;
       public          admin    false    202            e           0    0    todolist_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.todolist_id_seq OWNED BY public.todolist.id;
          public          admin    false    203            �
           2604    16399    todolist id    DEFAULT     j   ALTER TABLE ONLY public.todolist ALTER COLUMN id SET DEFAULT nextval('public.todolist_id_seq'::regclass);
 :   ALTER TABLE public.todolist ALTER COLUMN id DROP DEFAULT;
       public          admin    false    203    202            ]          0    16389    todolist 
   TABLE DATA           N   COPY public.todolist (id, list_number, list_name, list_completed) FROM stdin;
    public          admin    false    202   5       f           0    0    todolist_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.todolist_id_seq', 1, false);
          public          admin    false    203            �
           2606    16409 !   todolist todolist_list_number_key 
   CONSTRAINT     c   ALTER TABLE ONLY public.todolist
    ADD CONSTRAINT todolist_list_number_key UNIQUE (list_number);
 K   ALTER TABLE ONLY public.todolist DROP CONSTRAINT todolist_list_number_key;
       public            admin    false    202            �
           2606    16407    todolist todolist_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.todolist
    ADD CONSTRAINT todolist_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.todolist DROP CONSTRAINT todolist_pkey;
       public            admin    false    202            ]      x������ � �     