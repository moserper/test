const list_model = require("../models/list_model");
const { validationResult } = require("express-validator");
var count = 1;

function response(promise, res) {
	promise
		.then((result) => {
			if (result.data)
				res.status(result.status).json({ message: result.message, data: result.data });
			else
				res.status(result.status).json({ message: result.message });
		})
		.catch((err) => {
			res.status(err.status).json({ error: err.message });
		});
}

function inputValid(req) {
	const errors = validationResult(req);
	console.log(errors)
	if (!errors.isEmpty()) {
		return { valid: false, message: errors.array() };
	}
	return { valid: true, message: null };
}

function checkInput(req, res, model, ...input) {
	const { valid, message } = inputValid(req);
	if (valid)
		response(model(...input), res);
	else
		res.status(400).json(message);
}


module.exports = {
	createList: function (req, res) {
		checkInput(req, res, list_model.create, req.body.number, req.body.name);
	},
	removeList: function (req, res) {
		checkInput(req, res, list_model.remove, req.params.id);
	},
	editList: function (req, res) {
		// console.log(req.params);
		checkInput(req, res, list_model.edit, req.params.id, req.params.name);
	},
	completedList: function (req, res) {
		checkInput(req, res, list_model.completed, req.params.id, req.params.completed);
	},
	getToDoList: function (req, res) {
		response(list_model.getToDoList(), res);
	}
};
