const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const morgan = require("morgan");
const port = 5000;
const router = require("./router/router");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(morgan("combined"));

app.get("/", (req, res) => res.json({ message: "Hello World!" }));

app.use("/api", router);

app.use((req, res, next) => {
	const error = new Error(`Not found - ${req.originalUrl}`);
	res.status(404);
	next(error);
});

app.use((error, req, res, next) => {
	const statusCode = res.statusCode === 200 ? 500 : res.statusCode;
	res.status(statusCode);
	res.json({
		message: error.message,
	});
});

app.listen(port, () => {
	console.log(`Example app listening at http://localhost:${port}`);
});
