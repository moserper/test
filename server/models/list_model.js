const { pool } = require("../config/database_config");


const list_model = {
	table_name: "public.todolist",
	create: function (number, name) {
		return new Promise((resolve, reject) => {
			pool
				.query(
					`INSERT INTO ${list_model.table_name} (list_number, list_name, list_completed) VALUES (${number}, '${name}', false) RETURNING id`
				)
				.then((result) => {
					console.log(result);
					if (result.rowCount > 0)
						resolve({ status: 200, message: "create todolist success", data: result.rows[0] });
					else reject({ status: 500, message: "server error" });
				})
				.catch((err) => {
					if (err.name == "error" && err.routine == "_bt_check_unique")
						reject({ status: 400, message: "number is duplicate" });
					else reject({ status: 500, message: "create todolist fail" });
				});
		});
	},

	remove: function (id) {
		return new Promise((resolve, reject) => {
			pool
				.query(`DELETE FROM ${list_model.table_name} WHERE id = ${id}`)
				.then((result) => {
					console.log(result);
					if (result.rowCount > 0)
						resolve({ status: 200, message: "remove todolist success" });
					else reject({ status: 404, message: "not found id in todolist" });
				})
				.catch((err) => {
					reject({ status: 500, message: "server error" });
				});
		});
	},

	edit: function (id, name) {
		return new Promise((resolve, reject) => {
			pool.query(
				`UPDATE ${list_model.table_name} SET list_name = '${name}' WHERE id = ${id}`
			).then((result) => {
				if (result.rowCount > 0)
					resolve({ status: 200, message: "update edit name success" });
				else reject({ status: 500, message: "server error" });
			}).catch((err) => {
				console.log(err);
				reject({ status: 500, message: "update edit name fail" });
			});
		});
	},

	completed: function (id, completed) {
		return new Promise((resolve, reject) => {
			pool
				.query(
					`UPDATE ${list_model.table_name} SET list_completed=${completed} WHERE id = ${id}`
				)
				.then((result) => {
					if (result.rowCount > 0)
						resolve({ status: 200, message: "update completed success" });
					else reject({ status: 500, message: "server error" });
				})
				.catch((err) => {
					reject({ status: 500, message: "update completed fail" });
				});
		});
	},

	getToDoList: function () {
		return new Promise((resolve, reject) => {
			pool.query(`SELECT * FROM ${list_model.table_name} ORDER BY list_number ASC`)
				.then((result) => {
					if (result.rows.length > 0) resolve({ status: 200, message: "get todolist success", data: result.rows });
					if (result.rows.length == 0) resolve({ status: 200, message: "to do list empty" })
					else reject({ status: 500, message: "server error" })
				})
				.catch((err) => {
					reject({ status: 500, message: "server error" });
				});
		})
	}
};
module.exports = list_model;
