const router = require("express").Router();
const list_controller = require("../controllers/list_controller");
const { check } = require("express-validator");

router.post("/list/create", [check("number").isNumeric(), check("name").isString()], list_controller.createList);
// router.post("/list/create", list_controller.createList);
router.patch("/list/edit/:id/:name", [check("id").isNumeric(), check("name").isString()], list_controller.editList);
router.delete("/list/remove/:id", [check("id").isNumeric()], list_controller.removeList);
router.patch("/list/updateComplete/:id/:completed", [check("id").isNumeric(), check("completed").isBoolean()], list_controller.completedList);
router.get("/list/getToDoList", list_controller.getToDoList);

module.exports = router;
